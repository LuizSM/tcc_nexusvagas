<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculos', function (Blueprint $table) {
            $table->id();
            $table->string('nome', 50);
            $table->string('email',50);
            $table->date('data_nasc',50);
            $table->string('rua',100);
            $table->string('bairro', 100);
            $table->integer('numero');
            $table->string('cidade',100);
            $table->string('nacionalidade',50);
            $table->string('estado_civil',50);
            $table->string('formacao',150);
            $table->string('experiencia',150)->nullable()->default('Sem experiência');
            $table->string('habilidades',150);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculos');
    }
}
