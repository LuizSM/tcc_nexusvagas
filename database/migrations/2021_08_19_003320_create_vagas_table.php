<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vagas', function (Blueprint $table) {
            $table->id();

            $table->string('empresa',50)->nullable()->default('');
            $table->string('rua',100)->nullable()->default('');
            $table->string('bairro', 100)->nullable()->default('');
            $table->string('cidade')->nullable()->default('');
            $table->integer('numero')->nullable()->default(0);
            $table->string('cargo')->nullable()->default('');
            $table->string('salario')->nullable()->default('A combinar');
            $table->string('carga_horaria')->nullable()->default('');
            $table->string('carga_horaria2')->nullable()->default('');
            $table->longText('descricao')->nullable()->default('');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vagas');
    }
}
