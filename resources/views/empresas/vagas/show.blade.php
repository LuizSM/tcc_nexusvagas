@extends('empresas.layout.template')

@section('title', 'Home Empresa')

@section('conteudo')

@if ($mensagem = Session::get('mensagem'))
    <div class="alert alert-success">
        <p>{{ $mensagem }}</p>
    </div>
@endif
<div class="container">
<h1 class="display-1">{{$vaga->cargo}}</h1>
<p>Empresa: {{ $vaga->empresa }}</p>
<p>Endereço: Rua:{{ $vaga->rua }},Bairro:{{ $vaga->bairro }},Numero:{{ $vaga->numero }}</p>
<p>Cidade: {{ $vaga->cidade }}</p>
<p>Sálario:{{ $vaga->salario }}</p>
<p>Horário de entrada: {{ $vaga->carga_horaria }} &nbsp;&nbsp;Horário de saida: {{ $vaga->carga_horaria2 }}</p>
<p>Sobre a Vaga:{{ $vaga->descricao }}</p>

    <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Editar Vaga
    </button>
    <form onsubmit="return confirm('Tem certeza que quer apagar essa vaga?')" action="{{ route('vagas.destroy', $vaga->id) }}" method="POST">
        @csrf
        @method('delete')
        <button class="btn btn-danger" type="submit">Apagar</button>
    </form>
  <div class="collapse" id="collapseExample">
    <div class="card card-body">
      <hr>
    </div>
  </div>
  </p>
  <div class="collapse" id="collapseExample">
    <div class="container">
        <h1 class="display-4">Editando vaga de: {{ $vaga->cargo }}</h1>

@if ($errors->any())
    <strong>ATENÇÃO</strong> <p class="alert alert-danger">As informações inseridas não são válidas</p>

    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<form action="{{ route('vagas.update',$vaga->id) }}" method="POST">
    @method('put')
    @csrf
   <div class="form-group">
       <label for="empresa">Nome da empresa:</label>
       <input value="{{ $vaga->empresa ?? old('empresa') }}" class="form-control form-control-sm" name="empresa" type="text" id="empresa">
   </div>

   <div class="form-group">
       <label for="endereco">Endereço:</label>
           <input  value="{{ $vaga->rua ?? old('rua') }}" placeholder="Rua"    class="form-control form-control-sm mb-2" name="rua" type="text" id="rua">
           <input  value="{{ $vaga->bairro ?? old('bairro') }}" placeholder="Bairro" class="form-control form-control-sm mb-2" name="bairro" type="text" id="bairro">
           <input  value="{{ $vaga->numero ?? old('numero') }}" placeholder="Número" class="form-control form-control-sm" name="numero" type="number" id="numero">
   </div>

   <div class="form-group">
       <label for="cidade">Cidade:</label>
       <input value="{{ $vaga->cidade ?? old('cidade') }}" class="form-control form-control-sm" name="cidade" type="text" id="cidade">
   </div>

   <div class="form-group">
       <label for="cargo">Cargo:</label>
       <input value="{{ $vaga->cargo ?? old('cargo') }}" class="form-control form-control-sm" name="cargo" type="text" id="cargo">
   </div>

<div class="row">
   <div class="col">
       <div class="form-group">
           <label for="salario">Faixa salarial:</label>
           <input value="{{ $vaga->salario ?? old('salario') }}" maxlength="11" placeholder="R$1000 a R$1800" class="form-control form-control-sm" name="salario" type="text" id="salario">
       </div>
   </div>

   <div class="col">
       <div class="form-group">
           <label for="carga_horaria">Horário de entrada:</label>
           <input value="{{ $vaga->carga_horaria ?? old('carga_horaria') }}"  class="form-control form-control-sm" name="carga_horaria" type="time" id="carga_horaria">
       </div>
   </div>
   <div class="col">
       <div class="form-group">
           <label for="carga_horaria">Horário de saída:</label>
           <input value="{{ $vaga->carga_horaria2 ?? old('carga_horaria2') }}"  class="form-control form-control-sm" name="carga_horaria2" type="time" id="carga_horaria2">
       </div>
   </div>
</div>
       <div class="form-group">
           <label for="descricao">Descrição da vaga:</label>
           <textarea class="form-control form-control-sm" name="descricao" id="descricao" cols="30" rows="10">{{ $vaga->descricao ?? old('descricao') }}</textarea>
       </div>
       
       <button style="width: 100%" type="submit" class="btn btn-primary mt-2 mb-4">Atualizar Vaga</button>
   </form>
</div>
    </div>
</div>
</div>
@endsection
