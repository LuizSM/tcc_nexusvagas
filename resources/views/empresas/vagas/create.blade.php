@extends('empresas.layout.template')

@section('title', 'Criar Nova Vaga')

@section('conteudo')

<div id="navbar">
    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarnexus" aria-controls="navbarnexus" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarnexus">
          <div class="navbar-nav" id="navitens">
            <a class="nav-link active" aria-current="page" href="/home">Página Inicial</a>
            <a class="nav-link" href="/vagas/nova">Criar Vaga</a>
            <a class="nav-link" href="/gerenciar">Lista de vagas</a>
            <form method="POST" action="{{ route('logout') }}">
              @csrf
              <a class="nav-link":href="route('logout')"
              onclick="event.preventDefault();this.closest('form').submit();">
              Sair
            </a>
          </form>
            
            <a class="nav-link" href="/register" tabindex="-1" aria-disabled="true">Cadastro</a>
     
          </div>
        </div>
    </nav>     
</div>
<div class="container" id="container-create">
@if ($errors->any())
    <p class="alert alert-danger">As informações inseridas não são válidas</p>

    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
    
 <form action="{{ route('vagas.store') }}" method="POST">
     @csrf
    <div class="form-group">
        <label for="empresa">Nome da empresa:</label>
        <input value="{{ $vaga->empresa ?? old('empresa') }}" class="form-control form-control-sm" name="empresa" type="text" id="empresa">
    </div>

    <div class="form-group">
        <label for="endereco">Endereço:</label>
            <input  value="{{ $vaga->rua ?? old('rua') }}" placeholder="Rua"    class="form-control form-control-sm mb-2" name="rua" type="text" id="rua">
            <input  value="{{ $vaga->bairro ?? old('bairro') }}" placeholder="Bairro" class="form-control form-control-sm mb-2" name="bairro" type="text" id="bairro">
            <input  value="{{ $vaga->numero ?? old('numero') }}" placeholder="Número" class="form-control form-control-sm" name="numero" type="number" id="numero">
    </div>

    <div class="form-group">
        <label for="cidade">Cidade:</label>
        <input value="{{ $vaga->cidade ?? old('cidade') }}" placeholder="São Paulo-SP" class="form-control form-control-sm" name="cidade" type="text" id="cidade">
    </div>

    <div class="form-group">
        <label for="cargo">Cargo:</label>
        <input value="{{ $vaga->cargo ?? old('cargo') }}" class="form-control form-control-sm" name="cargo" type="text" id="cargo">
    </div>

 <div class="row">
    <div class="col">
        <div class="form-group">
            <label for="salario">Faixa salarial:</label>
            <input value="{{ $vaga->salario ?? old('salario') }}" maxlength="11" placeholder="R$1000 a R$1800" class="form-control form-control-sm" name="salario" type="text" id="salario">
        </div>
    </div>

    <div class="col">
        <div class="form-group">
            <label for="carga_horaria">Horário de entrada:</label>
            <input value="{{ $vaga->carga_horaria ?? old('carga_horaria') }}"  class="form-control form-control-sm" name="carga_horaria" type="time" id="carga_horaria">
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="carga_horaria">Horário de saída:</label>
            <input value="{{ $vaga->carga_horaria2 ?? old('carga_horaria2') }}"  class="form-control form-control-sm" name="carga_horaria2" type="time" id="carga_horaria2">
        </div>
    </div>
 </div>
        <div class="form-group">
            <label for="descricao">Descrição da vaga:</label>
            <textarea class="form-control form-control-sm" name="descricao" id="descricao" cols="30" rows="10">{{ $vaga->descricao ?? old('descricao') }}</textarea>
        </div>
        
        <button style="width: 100%" type="submit" class="btn btn-primary mt-2 mb-4">Anunciar Vaga</button>
    </form>
</div>

@endsection
