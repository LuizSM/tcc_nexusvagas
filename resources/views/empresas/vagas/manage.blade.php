@extends('empresas.layout.template')

@section('title', 'Home Empresa')

@section('conteudo')
<div class="container" id="container-lista">
<h1 class="display-5">Suas Vagas:
</h1>

<hr>

@if ($mensagem = Session::get('mensagem'))
    <div class="alert alert-danger">
        <p>{{ $mensagem }}</p>
    </div>
@endif

<div class="row">
    @foreach ($vagas as $vaga)
    <table class="table mb-4">
        <thead>
          <tr>
            <th scope="col">Cargo</th>
          </tr>
          <tbody>
              <td><p class="mb-4">{{$vaga->cargo}}</p>
                  <a class="btn btn-sm btn-dark" href="{{ route('vagas.show', $vaga->id) }}">Gerenciar</a>
              </td>
        </tbody>
      </table>
    @endforeach
</div>
</div>


@endsection