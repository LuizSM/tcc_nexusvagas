@extends('empresas.layout.template')

@section('title', 'Dashboard Empresa')

@section('conteudo')


<div class="container-fluid">
    <header class="row" id="header">
        <div id="logo-container">
          <a href="/"><img src="../img/logo.png" alt="logo"></a>
        </div>
    </header>
  </div>
  <!--NavBar-->
  <div id="navbar">
    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarnexus" aria-controls="navbarnexus" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarnexus">
          <div class="navbar-nav" id="navitens">
            <a class="nav-link active" aria-current="page" href="/home">Página Inicial</a>   
            <a class="nav-link" href="/register">Cadastro</a>
            <a class="nav-link" href="/login">Login</a>
          </div>
        </div>
    </nav>     
</div>


<div class="container" id="container-slide">
    <div class="carousel slide" id="slider" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../img/Banner1.jpg" alt="Banner Empresa" class="d-block w-100 img-fluid">
                    <div class="carousel-caption primary-bg-color">
                        <h5>Anuncie Suas vagas de Emprego</h5>
                        <p>Aqui tudo fica mais fácil</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/Banner2.jpg" alt="Banner Empresa 2" class="d-block w-100 img-fluid">
                    <div class="carousel-caption primary-bg-color">
                        <h5>Gerencie Tudo Facilmente</h5>
                        <p>Visualize edite e cadastre novas vagas</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/Banner3.jpg" alt="Banner Candidato 1" class="d-block w-100 img-fluid">
                    <div class="carousel-caption primary-bg-color">
                        <h5>Encontre vagas incríveis</h5>
                        <p>Inicie sua jornada no mercado de trabalho</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/Banner4.jpg" alt="Banner Candidato 2" class="d-block w-100 img-fluid">
                    <div class="carousel-caption primary-bg-color">
                        <h5>Encontre a vaga dos seus sonhos</h5>
                        <p>mude de vida</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" data-bs-target="#slider" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" data-bs-target="#slider" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">next</span>
            </button>
        </div>
@endsection