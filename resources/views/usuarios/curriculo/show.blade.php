@extends('empresas.layout.template')

@section('title', 'Home Empresa')

@section('conteudo')

@if ($mensagem = Session::get('mensagem'))
    <div class="alert alert-success">
        <p>{{ $mensagem }}</p>
    </div>
@endif
<div class="container">
<h1 class="display-1">{{$Curriculo->Nome}}</h1>
<p>Email: {{ $Curriculo->email }}</p>
<p>Endereço: Rua:{{ $Curriculo->rua }},Bairro:{{ $Curriculo->bairro }},Numero:{{ $Curriculo->numero }}</p>
<p>Cidade: {{ $Curriculo->cidade }}</p>
<p>Data de nascimento:{{ $Curriculo->data_nasc }}</p>

    <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Editar Vaga
    </button>
    <form onsubmit="return confirm('Tem certeza que quer apagar seu curriculo?')" action="{{ route('curriculos.destroy', $Curriculo->id) }}" method="POST">
        @csrf
        @method('delete')
        <button class="btn btn-danger" type="submit">Apagar</button>
    </form>
  <div class="collapse" id="collapseExample">
    <div class="card card-body">
      <hr>
    </div>
  </div>
  </p>
  <div class="collapse" id="collapseExample">
    <div class="container">
        <h1 class="display-4">Editando Curriculo de: {{ $Curriculo->nome }}</h1>

    @if ($errors->any())
        <p class="alert alert-danger">As informações inseridas não são válidas</p>
    
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
        
     <form action="{{ route('curriculos.store') }}" method="POST">
         @csrf
        <div class="form-group">
            <label for="nome">Nome:</label>
            <input value="{{ $curriculo->nome ?? old('nome') }}" class="form-control form-control-sm" name="nome" type="text" id="nome">
        </div>
    
        <div class="form-group">
            <label for="email">Email:</label>
            <input value="{{ $curriculo->email ?? old('email') }}" class="form-control form-control-sm" name="email" type="text" id="email">
        </div>
    
        <div class="form-group">
            <label for="data_nasc">Data de Nascimento:</label>
            <input value="{{ $curriculo->data_nasc ?? old('data_nasc') }}" class="form-control form-control-sm" name="data_nasc" type="date" id="data_nasc">
        </div>
    
        <div class="form-group">
            <label for="endereco">Endereço:</label>
                <input  value="{{ $curriculo->rua ?? old('rua') }}" placeholder="Rua"    class="form-control form-control-sm mb-2" name="rua" type="text" id="rua">
                <input  value="{{ $curriculo->bairro ?? old('bairro') }}" placeholder="Bairro" class="form-control form-control-sm mb-2" name="bairro" type="text" id="bairro">
                <input  value="{{ $curriculo->numero ?? old('numero') }}" placeholder="Número" class="form-control form-control-sm" name="numero" type="number" id="numero">
        </div>
    
        <div class="form-group">
            <label for="cidade">Cidade:</label>
            <input value="{{ $curriculo->cidade ?? old('cidade') }}"  class="form-control form-control-sm" name="cidade" type="text" id="cidade">
        </div>
    
        <div class="form-group">
            <label for="nacionalidade">Nacionalidade:</label>
            <input value="{{ $curriculo->nacionalidade ?? old('nacionalidade') }}" class="form-control form-control-sm" name="nacionalidade" type="text" id="nacionalidade">
        </div>
    
     <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="estado_civil">Estado Civil:</label>
                <input value="{{ $curriculo->estado_civil ?? old('estado_civil') }}" maxlength="11"  class="form-control form-control-sm" name="estado_civil" type="text" id="estado_civil">
            </div>
        </div>
    
        <div class="col">
            <div class="form-group">
                <label for="formacao">Formação:</label>
                <input value="{{ $curriculo->formacao ?? old('formacao') }}"  class="form-control form-control-sm" name="formacao" type="text" id="formacao">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="experiencia">Experiência:</label>
                <input value="{{ $curriculo->experiencia ?? old('experiencia') }}"  class="form-control form-control-sm" name="experiencia" type="text" id="experiencia">
            </div>
        </div>
     </div>
            <div class="form-group">
                <label for="habilidades">Habilidades:</label>
                <input value="{{ $curriculo->habilidades ?? old('habilidades') }}"  class="form-control form-control-sm" name="habilidades" type="text" id="habilidades">
            </div>
       <button style="width: 100%" type="submit" class="btn btn-primary mt-2 mb-4">Atualizar Vaga</button>
   </form>
</div>
    </div>
</div>
</div>
@endsection