<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VagaController;
use App\Http\Controllers\CurriculoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rotas para empresas
Route::group(['middleware' => ['auth', 'role:empresa']],function() {
    
    Route::get('/', [Vagacontroller::class, 'index'])->name('index');
    Route::get('/vagas/nova', [VagaController::class , 'create'])->name('vagas.create');
    Route::get('/vagas/{id}', [VagaController::class , 'show'])->name('vagas.show');
    Route::get('/editar/{id}', [VagaController::class, 'edit'])->name('vagas.edit');
    Route::get('/gerenciar', [VagaController::class, 'manage'])->name('vagas.manage');
    
    Route::put('/vagas/{id}', [VagaController::class, 'update'])->name('vagas.update');
    Route::delete('vagas/{id}',[VagaController::class, 'destroy'])->name('vagas.destroy');
    Route::post('/vagas/nova', [VagaController::class, 'store'])->name('vagas.store');
    
});


//Rotas para candidato
    Route::group(['middleware' => ['auth', 'role:candidato']],function() {

    Route::get('/', [Vagacontroller::class, 'index'])->name('index');
    Route::get('/curriculos/novo', [CurriculoController::class , 'create'])->name('curriculos.create');
    Route::get('/curriculos/{id}', [CurriculoController::class , 'show'])->name('curriculos.show');
    Route::get('/editar/{id}', [CurriculoController::class, 'edit'])->name('curriculos.edit');

    Route::put('/curriculos/{id}', [CurriculoController::class, 'update'])->name('curriculos.update');
    Route::delete('curriculos/{id}',[CurriculoController::class, 'destroy'])->name('curriculos.destroy');
    Route::post('/curriculos/novo', [CurriculoController::class, 'store'])->name('curriculos.store');
    });

    require __DIR__.'/auth.php';