<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;


class Curriculo extends Model
{
    protected $table = 'curriculos';
    protected $fillable = ['nome','email','data_nasc', 'rua', 'bairro', 'numero','cidade', 'nacionalidade', 'estado_civil', 'formacao','experiencia', 'habilidades'];

    use HasFactory;
    use softDeletes;
}
