<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class vaga extends Model
{

    protected $table = 'vagas';
    protected $fillable = ['empresa', 'rua', 'bairro', 'numero','cargo','cidade', 'periodo', 'salario', 'carga_horaria','carga_horaria2', 'descricao'];

    use HasFactory;
    use SoftDeletes;
}
