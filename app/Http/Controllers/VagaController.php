<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateVagas;
use App\Models\vaga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class VagaController extends Controller
{
    public function index(){
       if (auth::user()->hasRole('empresa')) {
            return view('empresas.vagas.index_empresa');
       }else if(auth::user()->hasRole('candidato')){
           return view('usuarios.curriculo.index_usuarios');
       }
    }

    public function create(){
        return view('empresas.vagas.create');
    }

    public function store(StoreUpdateVagas $request){
        $vaga = vaga::create($request->all());

        return redirect()
        ->route('vagas.show',$vaga->id)
        ->with ('mensagem', 'Salvo com sucesso');
    }

    public function show($id){
        $vaga = vaga::find($id);

        if(!$vaga)
            return redirect()->route('empresa_vagas.index');
            return view('empresas.vagas.show', compact('vaga'));
    }

    public function update(StoreUpdatevagas $request, $id){
        if (!$vaga = vaga::find($id))
            return redirect()->back();
        $vaga->update($request->all());

        return redirect()
            ->route('vagas.show', $vaga->id)
            ->with('mensagem', "Vaga editada com sucesso!");
    }

    public function edit($id){
        $vaga = vaga::find($id);

        if(!$vaga)
            return redirect()->back();
            
            return view('empresas.vagas.show', compact('vaga'));
    }

    public function destroy($id){
        $vaga = Vaga::all();
        $vaga = Vaga::find($id);

        if(!$vaga)
            return redirect()->route('vagas.manage');

        $vaga->delete();

        return redirect()
            ->route('vagas.manage')
            ->with('mensagem','Vaga deletada com sucesso!');
    }

    public function manage(){
        
        $vagas = vaga::all();
        return view('empresas.vagas.manage', compact('vagas'));
    }

}

