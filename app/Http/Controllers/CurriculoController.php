<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Curriculo;
use App\Http\Requests\StoreUpdateCurriculos;

class CurriculoController extends Controller
{
    public function create(){
        return view('usuarios.curriculo.create');
    }

    public function store(StoreUpdateCurriculos $request){
        $Curriculo = Curriculo::create($request->all());

        return redirect()
        ->route('curriculos.show',$Curriculo->id)
        ->with ('mensagem', 'Salvo com sucesso');
    }

    public function show($id){
        $Curriculo = Curriculo::find($id);

        if(!$Curriculo)
            return redirect()->route('index');
            return view('usuarios.curriculo.show', compact('Curriculo'));
    }

    public function update(StoreUpdateCurriculos $request, $id){
        if (!$Curriculo = Curriculo::find($id))
            return redirect()->back();
        $Curriculo->update($request->all());

        return redirect()
            ->route('Curriculos.show', $Curriculo->id)
            ->with('mensagem', "Curriculo editada com sucesso!");
    }

    public function edit($id){
        $Curriculo = Curriculo::find($id);

        if(!$Curriculo)
            return redirect()->back();
            
            return view('usuarios.Curriculo.show', compact('Curriculo'));
    }

    public function destroy($id){
        $Curriculo = Curriculo::all();
        $Curriculo = Curriculo::find($id);

        if(!$Curriculo)
            return redirect()->route('Curriculos.manage');

        $Curriculo->delete();

        return redirect()
            ->route('Curriculos.manage')
            ->with('mensagem','Curriculo deletada com sucesso!');
    }

}
